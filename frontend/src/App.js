import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import DeleteIcon from '@material-ui/icons/Delete';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListIcon from '@material-ui/icons/List';
import AddIcon from '@material-ui/icons/Add';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';

export class App extends Component {
    state = {
        drawer_open: false,
    };

    render() {
        return (
            <div style={{flexGrow: 1}}>
                <CssBaseline />

                {/* The AppBar */}
                <AppBar position="static">
                    <Toolbar>
                        <IconButton style={{marginLeft: -12, marginRight: 20}} color="inherit" onClick={() => this.setState({drawer_open: true})}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" style={{flexGrow: 1}}>
                            To Do List
                        </Typography>
                        <Button color="inherit">Sign Out</Button>
                    </Toolbar>
                </AppBar>

                {/* The Drawer */}
                <Drawer open={this.state.drawer_open} onClose={() => this.setState({drawer_open: false})}>
                    <div>
                        <ListItem button>
                            <ListItemIcon>
                                <ListIcon />
                            </ListItemIcon>
                            <ListItemText primary={"School List"} />
                        </ListItem>
                        <ListItem button>
                            <ListItemIcon>
                                <ListIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Work List"} />
                        </ListItem>
                        <ListItem button>
                            <ListItemIcon>
                                <ListIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Shopping List"} />
                        </ListItem>
                        <ListItem button>
                            <ListItemIcon>
                                <AddIcon />
                            </ListItemIcon>
                            <ListItemText primary="Create List" />
                        </ListItem>
                    </div>
                </Drawer>

                {/* The List */}
                <div style={{display: 'flex', justifyContent: 'center'}}>
                    <Paper style={{maxWidth: '1020px', width: '100%', marginTop: '40px'}}>
                        <div>
                            <div style={{display: 'flex', padding: '24px 24px'}}>
                                <Typography variant="headline" style={{flexGrow: 1}}>To Do List</Typography>
                                <Button color="secondary" variant="raised" size="small">Delete List</Button>
                            </div>
                            <ListItem>
                                <ListItemText primary={<TextField autoFocus placeholder="Type a new task" style={{width: '100%'}}/>} />
                                <ListItemIcon><IconButton><AddIcon/></IconButton></ListItemIcon>
                            </ListItem>
                            <ListItem>
                                <ListItemIcon><Checkbox/></ListItemIcon>
                                <ListItemText primary={"Cheerios"}/>
                                <IconButton><DeleteIcon/></IconButton>
                            </ListItem>
                            <ListItem>
                                <ListItemIcon><Checkbox/></ListItemIcon>
                                <ListItemText primary={"Apples"}/>
                                <IconButton><DeleteIcon/></IconButton>
                            </ListItem>
                        </div>
                    </Paper>
                </div>
            </div>
        );
    }
}